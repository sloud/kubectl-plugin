install_kubectl() {
    local whereami="$1"
    local bin="$HOME/.local/bin"
    local completions_dir="$whereami/src"

    if ! type kubectl 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl -L "https://dl.k8s.io/release/v1.25.0/bin/linux/amd64/kubectl" >"$bin/kubectl"
        chmod +x "$bin/kubectl"

        mkdir -p "$completions_dir"
        kubectl completion zsh >"$completions_dir/_kubectl"
    fi

    fpath+="$completions_dir"
}

install_kubectl "${0:A:h}"
unset install_kubectl
